package selectGiftCard;

import org.junit.Test;
//import org.testng.annotations.Test;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import navigation.GoTo;
import actions.PricePage;
import actions.CartPage;

import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.testng.Assert.assertEquals;

public class AddGiftCard extends SteviaTestBase{

    @Autowired
    private GoTo goTo;

    @Autowired
    private PricePage pricePage;

    @Autowired
    private CartPage cartPage;

    @Test
    @DisplayName("This scenario select a product and add it to cart. Ensure that the price is correct")
    public void testAddCardPrice() {
        //Click cart
        goTo.clickGiftCards();
        //Click Print at Home
        goTo.clickPrintHome();
        //Select standard design
        pricePage.selectedStandard();
        //Select third card
        pricePage.selectThirdCard();
        //or to select the third card of the list of cards
        //pricePage.clickCard(3);

        //Get selected card price
        String selectedPrice = pricePage.selectedAmount();
        //Add to cart
        pricePage.addToCart();
        //Go to cart
        goTo.clickCart();
        //Verify the price is the same with selected one
        String productPrice = cartPage.getPrice();

        String errorMessage = "The price in cart is not equal to the product price";
        assertEquals(productPrice, selectedPrice, errorMessage);
    }


}
