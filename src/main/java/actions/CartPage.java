package actions;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class CartPage extends WebComponent {
    public enum Locators {
        PRICE("[data-csa-c-slot-id=\"nav_cs_3\"]");

        private String myLocator;

        Locators(String locator) {
            myLocator = locator;
        }

        public String get(){
            return myLocator;
        }
    }
    public String getPrice() {
        return controller().getText(CartPage.Locators.PRICE.get());
    }
}