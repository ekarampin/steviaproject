package actions;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PricePage extends WebComponent {
    private enum Locators {
        SELECTED_PRICE("[id='gc-amount-mini-picker']>.gc-mini-amount2"),
        STANDARD("[id=\"gc-customization-type-button-Designs\"]"),
        DESIGN_LIST(),
        SELECT_THIRD_CARD("[id=\"a-autoid-11\"]"),
        ADD_TO_CART("input[id=\"gc-buy-box-atc\"][name=\"submit.gc-add-to-cart\"]");

        private String myLocator;

        Locators(String locator) {
            myLocator = locator;
        }

        public String get(){
            return myLocator;
        }
    }

    public void selectedStandard(){
        controller().getText(Locators.STANDARD.get());
    }

    public String selectedAmount(){
        String price = controller().getText(Locators.SELECTED_PRICE.get());
        return price;
    }

    public void selectThirdCard(){
        controller().getText(Locators.SELECT_THIRD_CARD.get());
    }

    private WebElement selectCard(int num) {
        List<WebElement> products = controller().findElements(Locators.DESIGN_LIST.get());
        return null;
    }

    public void clickCard(int number) {
        WebElement product = selectCard(number);
        product.click();
    }

    public void addToCart(){
        controller().getText(Locators.ADD_TO_CART.get());
    }
}
