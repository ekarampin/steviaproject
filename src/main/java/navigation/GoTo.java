package navigation;
import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class GoTo extends WebComponent {
    public enum Locators {
        GIFT_CARDS_BAR("[data-csa-c-slot-id=\"nav_cs_3\"]"),
        PRINT_AT_HOME("[aria-label=\"Print at home\"]"),
        CART_BAR("[id=\"nav-cart-count\"]");

        private String myLocator;

        Locators(String locator) {
            myLocator = locator;
        }

        public String get(){
            return myLocator;
        }
    }
    public void clickGiftCards(){
        controller().click(Locators.GIFT_CARDS_BAR.get());
    }

    public void clickPrintHome(){
        controller().click(Locators.PRINT_AT_HOME.get());
    }

    public void clickCart(){
        controller().click(Locators.CART_BAR.get());
    }

}
