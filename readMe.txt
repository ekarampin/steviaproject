Files:
pom.xml:
In this file I added the dependency with the Stevia project.

testng.xml:
Unfortunately i could not create the file, so based on "stevia 10 minutes Quick Start"
it would be something like this. In this testng.xml file we declare informations about the test like:
the url, the browser, the driverType and the listeners.


src > test > java > myPackage > myTest.java:
In this file there is the test. There are imported all the required classes. At the end of the test I use "assertEquals"
method to compare the two prices.
Moreover I should import "org.testng" but I was not able to set it correct.

src > main > java > myPackage > Page Objects:
In the files, I extend the stevia "WebComponent" class.
I tried to separate the files according to the functionality of each class.
Note: I tried to give the user the opportunity to give as input the number of the card that he wants to test it. (List of WebElement)